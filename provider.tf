terraform {
  backend "s3" {
    bucket = "andrzejczak.dev-terraform"
    key    = "prod/terraform.tfstate"
    region = "eu-central-1"
  }
}

provider "aws" { 
  region = "eu-central-1"
  profile = "terraform"
}

provider "aws" {
  alias  = "acm_provider"
  region = "us-east-1"
  profile = "terraform"
}